import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import IconButton from "@material-ui/core/IconButton";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import FormDialog from "./dialog.component";
import DeleteDialog from "./deleteDialog.component";

const useStyles = makeStyles({
  root: {
    width: "50%",
    minWidth: "345px",
    marginBottom: 30,
    margin: "auto",
  },
  media: {
    height: 140,
  },
});

export default function MediaCard(props) {
  const classes = useStyles();

  const { duty, setDutyList, dutyList } = props;

  const [showDialog, setShowDialog] = useState(false);
  const [showDeleteDialog, setShowDeleteDialog] = useState(false);

  const handleEditClick = () => {
    setShowDialog(true);
  };

  const handleDeleteClick = () => {
    setShowDeleteDialog(true);
  };

  const borderStyle = {
    borderTop: `10px solid ${duty.color} `,
    marginTop: "40px",
  };

  return (
    <>
      {showDialog && (
        <FormDialog
          duty={duty}
          open={showDialog}
          edit={true}
          setShowDialog={setShowDialog}
          setDutyList={setDutyList}
          dutyList={dutyList}
        />
      )}
      {showDeleteDialog && (
        <DeleteDialog
          id={duty._id}
          open={showDeleteDialog}
          setShowDeleteDialog={setShowDeleteDialog}
          setDutyList={setDutyList}
          dutyList={dutyList}
        />
      )}
      <Card className={classes.root} style={borderStyle}>
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {duty.id}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {duty.name}
            </Typography>
          </CardContent>
          <CardActions disableSpacing>
            <IconButton
              component="div"
              aria-label="edit"
              onClick={handleEditClick}
            >
              <EditIcon />
            </IconButton>
            <IconButton
              component="div"
              aria-label="edit"
              onClick={handleDeleteClick}
            >
              <DeleteIcon />
            </IconButton>
          </CardActions>
        </CardActionArea>
      </Card>
    </>
  );
}
