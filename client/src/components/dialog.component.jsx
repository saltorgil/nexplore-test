import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { DutyService } from "../../src/services/duty";
import randomColor from "randomcolor";

export default function FormDialog(props) {
  /** Services */
  const dutyService = new DutyService();

  /** States */
  const { open, duty, edit, setShowDialog, dutyList, setDutyList } = props;

  /** Form */
  const [formData, setFormData] = useState({
    id: duty && duty.id ? duty.id : "",
    name: duty && duty.name ? duty.name : "",
  });

  /** Handlers */
  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleClose = () => {
    setShowDialog(false);
  };

  const handleSave = async () => {
    try {
      await dutyService.updateDuty({ ...formData, _id: duty._id }, duty._id);
      const updatedList = dutyList.map((item) =>
        item._id === duty._id ? { ...formData, color: item.color } : item
      );
      setDutyList(updatedList);
      setShowDialog(false);
    } catch (e) {
      console.error("Error updating duty", e);
    }
  };

  const handleCreate = async () => {
    try {
      const response = await dutyService.createDuty(formData);
      setShowDialog(false);
      setDutyList([...dutyList, { ...response, color: randomColor() }]);
    } catch (e) {
      console.error("Error creating duty", e);
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {edit ? "Edit" : "Create"}
        </DialogTitle>
        <DialogContent>
          <TextField
            name="id"
            label="Id"
            fullWidth
            value={formData.id}
            onChange={handleInputChange}
            margin="dense"
          />
          <TextField
            name="name"
            label="Name"
            fullWidth
            value={formData.name}
            onChange={handleInputChange}
            margin="dense"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={edit ? handleSave : handleCreate} color="primary">
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
