import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { DutyService } from "../../src/services/duty";

export default function DeleteDialog(props) {
  /** Services */
  const dutyService = new DutyService();

  /** UseState */
  const { open, setShowDeleteDialog, id, setDutyList, dutyList } = props;

  const handleClose = () => {
    setShowDeleteDialog(false);
  };

  /** Handlers */
  const handleDelete = async () => {
    try {
      await dutyService.deleteDuty(id);
      setDutyList(dutyList.filter((item) => item._id !== id));
      setShowDeleteDialog(false);
    } catch (e) {
      console.error("Error updating duty", e);
    }
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure to delete this item?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleDelete} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
