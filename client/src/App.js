import "./App.css";
import { DutyService } from "../src/services/duty";
import React, { useEffect, useState } from "react";
import Card from "./components/card.component";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import FormDialog from "./components/dialog.component";
import randomColor from "randomcolor";

const useStyles = makeStyles({
  root: {
    fontFamily: "'Roboto Condensed'",
  },
  titleGrid: {
    marginTop: 50,
    marginBottom: 50,
    textAlign: "center",
  },
  title: {
    fontWeight: "bold",
  },
  subTitle: {
    fontWeight: "lighter",
    color: "#666666",
  },
});

function App() {
  /** Styles */
  const classes = useStyles();

  /** Services */
  const dutyService = new DutyService();

  /** UseState */
  const [dutyList, setDutyList] = useState([]);
  const [showDialog, setShowDialog] = useState(false);

  /** UseEffect */
  const getDutyList = async () => {
    try {
      const dutyList = await dutyService.getDuty();
      dutyList &&
        setDutyList(
          dutyList.map((item) => ({ ...item, color: randomColor() }))
        );
    } catch (e) {
      console.error("Error getting TodoList", e);
    }
  };

  /** Handlers */
  const handleCreateClick = () => {
    setShowDialog(true);
  };

  useEffect(() => {
    getDutyList();
  }, []);

  return (
    <Grid container className={classes.root} justifyContent="center">
      {showDialog && (
        <FormDialog
          duty={null}
          open={showDialog}
          edit={false}
          setShowDialog={setShowDialog}
          dutyList={dutyList}
          setDutyList={setDutyList}
        />
      )}
      <Grid
        container
        className={classes.titleGrid}
        direction="column"
        alignItems="center"
      >
        <Typography variant="h3" className={classes.title}>
          To-do List
        </Typography>
        <Typography variant="h5" className={classes.subTitle}>
          Here you can edit, add or delete to-do items
        </Typography>
      </Grid>

      <Grid container justifyContent="center" onClick={handleCreateClick}>
        <AddIcon fontSize="large"></AddIcon>
      </Grid>

      <Grid container>
        {dutyList && dutyList.length
          ? dutyList.map((item) => (
              <Grid key={item._id} item xs={12} md={4} lg={3}>
                <Card
                  duty={item}
                  setDutyList={setDutyList}
                  dutyList={dutyList}
                />
              </Grid>
            ))
          : null}
      </Grid>
    </Grid>
  );
}

export default App;
