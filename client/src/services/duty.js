import axios from "axios";

const API_URL = "http://localhost:5000/todos/";

export class DutyService {
  getDuty = async () => {
    try {
      const response = await axios.get(API_URL);
      return response.data;
    } catch (err) {
      console.error(err);
    }
  };

  updateDuty = async (duty, id) => {
    try {
      const response = await axios.put(`${API_URL}${id}`, duty);
      return response.data;
    } catch (err) {
      console.error(err);
    }
  };

  createDuty = async (duty, id) => {
    try {
      const response = await axios.post(`${API_URL}`, duty);
      return response.data;
    } catch (err) {
      console.error(err);
    }
  };

  deleteDuty = async (id) => {
    try {
      const response = await axios.delete(`${API_URL}${id}`);
      return response.data;
    } catch (err) {
      console.error(err);
    }
  };
}
