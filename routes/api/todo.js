const express = require("express");
const router = express.Router();

const Duty = require("../../models/Duty");

router.get("/", async (req, res) => {
  try {
    const duties = await Duty.find();
    res.json(duties);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Server Error");
  }
});

router.post("/", async (req, res) => {
  try {
    const newDuty = new Duty({
      id: req.body.id,
      name: req.body.name,
    });
    const duty = await newDuty.save();
    res.json(duty);
  } catch (error) {
    console.error(error.message);
    res.status(500).send(error.message);
  }
});

router.put("/:id", async (req, res) => {
  try {
    const id = { _id: req.params.id };
    const update = { id: req.body.id, name: req.body.name };

    const updatedDuty = await Duty.findByIdAndUpdate(id, update, {
      new: true,
    });

    res.json(updatedDuty);
  } catch (error) {
    console.error(error.message);
    res.status(500).send(error.message);
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const duty = await Duty.findById(req.params.id);

    if (!duty) {
      return res.status(404).json({ msg: "Duty not found" });
    }

    await duty.remove();
    res.json({ msg: "Duty removed" });
  } catch (error) {
    console.error(error.message);
    console.log(error);

    res.status(500).send("Server Error");
  }
});

module.exports = router;
