const mongoose = require("mongoose");

const DutySchema = new mongoose.Schema({
  id: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
});

module.exports = Duty = mongoose.model("duty", DutySchema);
