# Nexplore-test:

Its an end-to-end web application that allows
the user to read, create and update a to-do list of duties of any kind.
Developed with:

- Server: NodeJS with Express and MongoDB with Mongoose (hosted at https://cloud.mongodb.com/)
- Client: ReactJS with Material-ui for styles at frontend

# Usage

Local environment:

- npm install
- (cd /client) npm install
- npm run dev

# Demo

https://limitless-forest-59371.herokuapp.com/
